package classes.cyber.money.example.card;

import classes.cyber.money.example.account.Account;
import classes.cyber.money.example.account.BasicAccount;
import classes.cyber.money.example.account.NotEnoughMoneyException;

public class BankCard { // may be extended by DepositCard / CreditCard etc...

    public int number;
    //public int expireMonth;
    //public int expireYear;
    //public String cardholder;
    //public int cvv;
    protected int pin;
    protected boolean isPINVerified = false;

    protected Account account;

    public BankCard(int number, int pin) {
        this.number = number;
        this.pin = pin;
        account = new BasicAccount();
    }

    public boolean verifyPIN(int pin) { // may be it should be abstract???
        boolean isVerified = this.pin == pin;
        isPINVerified = isVerified;
        return isVerified;
    }

    public double withdrawMoney(double amount) throws NotEnoughMoneyException, PINNotVerifiedException {
        if (isPINVerified) {
            isPINVerified = false;
            return account.withdrawMoney(amount);
        } else {
            throw new PINNotVerifiedException();
        }
    }
}
