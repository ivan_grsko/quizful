package classes.cyber.money.example.card;

import classes.cyber.money.example.account.BasicAccount;

public class BankCardBOC extends BankCard {
    BasicAccount account;

    public BankCardBOC(int number, int pin) {
        super(number, pin);
    }
}
