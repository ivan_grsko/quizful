package classes.cyber.money.example.tests;

import classes.cyber.money.example.atm.WithdrawATM;
import classes.cyber.money.example.card.BankCard;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class WithdrawATMTest {

    WithdrawATM atm;
    int pin;
    BankCard card;

    private void renewTestData() {
        atm = new WithdrawATM();
        pin = 374;
        card = new BankCard(777, pin);
    }

    @BeforeMethod
    public void beforeEveryTest() {
        renewTestData();
    }

    @Test
    public void verifyPINTest() {
        boolean isInitSuccessful = atm.initCard(card);
        Assert.assertEquals(isInitSuccessful, true);

        boolean isPINCorrect = atm.verifyPIN(1374);
        Assert.assertEquals(isPINCorrect, false);

        isPINCorrect = atm.verifyPIN(374);
        Assert.assertEquals(isPINCorrect, true);
    }

    @Test
    public void withdrawWhenNotEnoughMoneyTest() {
        atm.initCard(card);
        atm.verifyPIN(pin);
        long withdraw = atm.withdraw(1);

        Assert.assertEquals(withdraw, 0L);
    }

    @Test
    public void withdrawWithBadPINTest() {
        atm.initCard(card);
        atm.verifyPIN(pin + 1);
        long withdraw = atm.withdraw(1);

        Assert.assertEquals(withdraw, 0L);
    }

    /*@Test
    public void depositTest() {
        atm.initCard(card);
        atm.verifyPIN(pin + 1);
        long withdraw = atm.deposit(100);

        Assert.assertEquals(withdraw, 1000L);
    }*/

}
