package classes.cyber.money.example.atm;


import classes.cyber.money.example.card.BankCard;

// Automated teller machine
public abstract class ATM {

    protected BankCard bankCardInserted;
    protected boolean isPINVerified = false;


    public abstract boolean initCard(BankCard card);

    public boolean verifyPIN(int pin) {
        // verifyPIN()??? is it method of ATM or Card??? Of Card because "pin entered into ATM is temporary" but that one in card is nearly "constant"
        return bankCardInserted.verifyPIN(pin);
    }

    public abstract long withdraw(long withdrawAmount); // Long or long??? should I return NULL? Or should throw custom exception NotEnoughMoneyException & handle it by another method...

    public BankCard returnCard() {
        isPINVerified = false;
        BankCard bankCardInserted = this.bankCardInserted;
        this.bankCardInserted = null;
        return bankCardInserted;
    }
}
