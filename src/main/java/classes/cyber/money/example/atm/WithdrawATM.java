package classes.cyber.money.example.atm;

import classes.cyber.money.example.account.NotEnoughMoneyException;
import classes.cyber.money.example.card.BankCard;
import classes.cyber.money.example.card.PINNotVerifiedException;

public class WithdrawATM extends ATM {

    public boolean initCard(BankCard card) {

        bankCardInserted = card;

        // establish connection with Bank

        // should be in another class e.g. ATMInterface whatever
        /*Scanner sc = new Scanner(System.in);
        System.out.println("Enter pin: ");
        String pin = sc.next();*/



        return true;
    }

    public long withdraw(long withdrawAmount) {
        double withdrawnMoney = 0;
        try {
            withdrawnMoney = bankCardInserted.withdrawMoney((double) withdrawAmount);
        } catch (NotEnoughMoneyException e) {
            System.out.println("Not enough money to complete operation. Please try again with another amount.");
            return 0;
        } catch (PINNotVerifiedException e) {
            // need to handle 1) 3 attempts is a !row! 2) navigation > pin entering | return card | ?another operation?
            //pinRequestScreen();
            System.out.println("PIN is not verified. Please go to PIN enter screen.");
            return 0;
        }
        return (long)withdrawnMoney;
    }
}
