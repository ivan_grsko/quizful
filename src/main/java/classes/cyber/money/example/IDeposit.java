package classes.cyber.money.example;

public interface IDeposit {
    double rate = 0.0;
    double monthlyInterest();
}
