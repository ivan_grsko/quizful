package classes.cyber.money.example.account;

public class BasicAccount extends Account {

    @Override
    synchronized
    public boolean isAmountAvailable(double amount) {
        return this.amount >= amount;
    }

    @Override
    synchronized
    public double withdrawMoney(double amount) throws NotEnoughMoneyException {
        if (isAmountAvailable(amount)) {
            this.amount -= amount;
            return amount;
        } else {
            throw new NotEnoughMoneyException();
        }
    }

    @Override
    synchronized
    public double depositMoney(double amount) {
        this.amount += amount;
        return this.amount;
    }
}
