package classes.cyber.money.example.account;

public abstract class Account {
    // could be extended by CreditAccount, DepositAccount, MultiCurrencyAccount
    protected double amount;
    public abstract boolean isAmountAvailable(double amount);

    // returns withdrawn amount of money (as it is usually printed out on receipt)
    public abstract double withdrawMoney(double amount) throws NotEnoughMoneyException;
    // returns total amount of money (as it is usually printed out on receipt)
    public abstract double depositMoney(double amount);

}
