package classes.finals;


public class B { // extends AFinal { --- causes: Cannot inherit from final 'classes.finals.AFinal'
    public int i;

    // \/ code below is not compiled:
    //public final int f;     // Variable 'f' might not have been initialized

    // it should be
    public final int f = 3;
}
