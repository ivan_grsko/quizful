package classes.finals;

import org.testng.annotations.Test;

public class Testing {

    @Test
    public void test() {
        final AFinal a = new AFinal();
        //a = null;           // IMpossible
        //a = new AFinal();   // IMpossible
        a.i = 10;       // possible - fields of final class' instance is still editable

        B b = new B();
        b = null;       // possible
        b = new B();    // possible
    }

}
