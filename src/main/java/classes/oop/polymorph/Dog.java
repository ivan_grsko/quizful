package classes.oop.polymorph;

public class Dog extends Animal {

    @Override
    public void doSound() {
        System.out.println("The dog says: bow wow");
    }

}
