package classes.oop.polymorph;

import org.testng.annotations.Test;

public class TheTest {

    @Test
    public void test() {
        Animal animal = new Animal();
        Pig pig = new Pig();
        Dog dog = new Dog();

        animal.doSound();
        pig.doSound();
        dog.doSound();

        // possibility to call implementation of child class by the interface of the parent (LATE BINDING)
        Animal.doSound(animal);
        Animal.doSound(pig);    // wee wee
        Animal.doSound(dog);    // bow wow

        Animal animalOne = (Animal)pig; // redundant casting
        Animal animalTwo = (Animal)dog; // redundant casting

        animal.doSound();
        animalOne.doSound();    // wee wee
        animalTwo.doSound();    // bow wow

        Animal.doSound(animal);
        Animal.doSound(animalOne);    // wee wee
        Animal.doSound(animalTwo);    // bow wow
    }

}
