package classes.oop.polymorph;

public class Animal {
    // it's better to make this class or just method doSound() abstract
    // cause actually we can't make specify the sound animal do unless we name it

    public void doSound() {
        System.out.println(" --- Animal sound ---");
    }

    public static void doSound(Animal animal) {
        animal.doSound();
    }

}
