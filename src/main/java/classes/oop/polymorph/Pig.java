package classes.oop.polymorph;

public class Pig extends Animal {

    @Override
    public void doSound() {
        System.out.println("The pig says: wee wee");
    }
}
