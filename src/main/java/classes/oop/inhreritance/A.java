package classes.oop.inhreritance;

public class A {

    public int aVal;

    public void aMethod() {
        System.out.println("aMethod(); aVal = " + aVal);
    }

    public A() { aVal = 777; }
    public A(int i) { aVal = i; }

}
