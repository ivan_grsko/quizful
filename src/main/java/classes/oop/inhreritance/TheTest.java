package classes.oop.inhreritance;

import org.testng.annotations.Test;

public class TheTest {

    @Test
    public void test() {
        A a = new A();
        B b = new B();


        // variable & method
        a.aVal = 1;
        a.aMethod();

        b.aVal = 2;
        b.bVal = 3;
        b.aMethod();
        b.bMethod();

        // constructor
        System.out.println(" --- constructor ---");
        a = new A(1);
        //b = new B(2); // !!! Parameterized constructors are NOT inherited !!!

        b = new B();
        b.aMethod();    // aVal = 777 --- but this constructor's functionality is inherited
        b.bMethod();    // aVal = 0

        //b = (B)new A();   // Execution error: java.lang.ClassCastException: class classes.oop.inhreritance.A cannot be cast to class classes.oop.inhreritance.B (classes.oop.inhreritance.A and classes.oop.inhreritance.B are in unnamed module of loader 'app')

    }

}
