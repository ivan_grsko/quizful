package classes.oop.inhreritance;

public class B extends A {

    public int bVal;

    public void bMethod() {
        System.out.println("bMethod(); bVal = " + bVal);
    }

    // !!! inherits only constructor A() = B(), not A(int)
    // definition of this constructor MAKE B() = A() impossible to use, now need B() to be defined here
    //public B(double d) { bVal = (int) d; }
}
