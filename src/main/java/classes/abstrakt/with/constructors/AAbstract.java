package classes.abstrakt.with.constructors;

public abstract class AAbstract {

    // Abstruct class can have constructors
    public AAbstract() {

    }

    // Abstruct class can NOT have ABSTRACT constructors
    // public abstract AAbstract(int i);
}
