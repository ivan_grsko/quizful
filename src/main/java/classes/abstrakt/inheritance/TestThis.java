package classes.abstrakt.inheritance;

import org.testng.annotations.Test;

public class TestThis {

    @Test
    public void test() {
        AAbstract a = new AAbstract() {
            @Override
            public void printAbstractMethod() {
                System.out.println(" + I have to override abstract method to create instance of AAbstract right during the declaration.");
            }
        };
        a.callAllPrintMethods();


        B b = new B();
        b.callAllPrintMethods();


        CAbstract c = new CAbstract() {
            @Override
            public void printAbstractMethod() {
                System.out.println(" + I had to override abstract print method AGAIN!!! to create instance of CAbstract :)");
            }
        };
        c.callAllPrintMethods();
    }
}
