package classes.abstrakt.inheritance;

public abstract class CAbstract extends B {
    // SO:
    // Abstract class can extend not-abstract class
    // And! You don't need to write anything here!!!



    // But still let's try to make printAbstractMethod() implemented in B abstract again...
    public abstract void printAbstractMethod();
    // Yeah!!! That great! It is abstract again!!! No implementation is inherited from B!!!
}
