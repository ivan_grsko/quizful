package classes.abstrakt.inheritance;

public class B extends AAbstract {

    // Non-abstract class must implement all abstract methods of abstract class that is extended
    public void printAbstractMethod() {
        System.out.println(" + abstract print method of A is implemented in B (Abstract method must be implemented in non-abstract class that extends abstract)");
    }
}
