package classes.abstrakt.inheritance;

public abstract class AAbstract {
    public abstract void printAbstractMethod();

    public void printNonAbstractMethod(){
        System.out.println(
                " + __Implemented__ Print Method Of AAbstract class. " +
                        "Abstract class can have non abstract methods.");
    }

    public void callAllPrintMethods() {
        System.out.println("---=== Call all print methods of AAbstract ===---");
        printAbstractMethod();
        printNonAbstractMethod();
        System.out.println("---=== END: Call all print methods of AAbstract ===---\n\n");
    }
}
