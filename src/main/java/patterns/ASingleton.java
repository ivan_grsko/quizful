package patterns;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ASingleton {

    // region Lazy init singleton (NOT thread safe)
    private static ASingleton singleInstance;
    private String name;

    private ASingleton() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        name = formatter.format(LocalTime.now());
    }

    public static ASingleton getInstance() {
        if (singleInstance == null) {
            singleInstance = new ASingleton();
        }
        return singleInstance;
    }
    // endregion


    @Override
    public String toString() {
        return name;
    }
}
