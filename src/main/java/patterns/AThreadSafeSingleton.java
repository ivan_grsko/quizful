package patterns;

import org.testng.annotations.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


public class AThreadSafeSingleton {

    // region thread safe getInstance() is made synchronized so that it could not be accessed simultaneously by
    // different threads
    // BUT: it's better to make synchronized block of second instance is null check to significantly improve performance

    private static AThreadSafeSingleton singleInstance;
    private String name;

    private AThreadSafeSingleton() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        name = formatter.format(LocalTime.now());
    }

    public static AThreadSafeSingleton getInstance() {
        if (singleInstance == null) {
            synchronized (AThreadSafeSingleton.class) {
                if (singleInstance == null) {
                    singleInstance = new AThreadSafeSingleton();
                }
            }
        }
        return singleInstance;
    }

    @Override
    public String toString() {
        return name;
    }

    // endregion

}
