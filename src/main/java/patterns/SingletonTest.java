package patterns;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SingletonTest {

    @Test
    public void simpleSingletonIdentityTest() {
        ASingleton instanceOne = ASingleton.getInstance();
        System.out.println(instanceOne);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ASingleton instanceTwo = ASingleton.getInstance();
        System.out.println(instanceTwo);

        Assert.assertEquals(instanceOne.toString().equals(instanceTwo.toString()), true);
        Assert.assertEquals(instanceOne.equals(instanceTwo), true);
    }

    @Test
    public void treadSafeSingletonIdentityTest() {
        // still no check of thread safety

        AThreadSafeSingleton instanceOne = AThreadSafeSingleton.getInstance();
        System.out.println(instanceOne);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        AThreadSafeSingleton instanceTwo = AThreadSafeSingleton.getInstance();
        System.out.println(instanceTwo);

        Assert.assertEquals(instanceOne.toString().equals(instanceTwo.toString()), true);
        Assert.assertEquals(instanceOne.equals(instanceTwo), true);
    }

}
