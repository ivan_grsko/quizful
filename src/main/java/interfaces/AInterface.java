package interfaces;

public interface AInterface {

    // why it must be assigned? This is not allowed: int i;
    Integer i = 0;

    // does not have ACCESS MODIFIERs (public, private, protected)
    void print();
    // BUT it is implicitly meant to have "public" & "abstract":
    public abstract void printMore();
}
