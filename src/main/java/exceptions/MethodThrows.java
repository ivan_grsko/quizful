package exceptions;

import java.io.IOException;

public class MethodThrows {

    // Method can throw unchecked exceptions like IllegalArgumentException
    // & it should not allow to throw two identical exceptions - still NO compilation error
    public void method() throws
            IllegalArgumentException, IllegalArgumentException, // duplicates of exception are allowed
            IllegalStateException, IOException, // can mix checked & unchecked exceptions
            IOException {

        // body...

    }
}
