package bit.operations;

import org.testng.annotations.Test;

public class Calculations {

    @Test
    public void calculationOneTest() {
        int a = 0xFF0; // 1111 1111 0000
        int b = 0xF0F; // 1111 0000 1111
        int c = 0x0FF; // 0000 1111 1111

        System.out.println(Integer.toBinaryString(a & b)); // 1111 0000 0000
        System.out.println(Integer.toBinaryString(c | a & b)); // 1111 1111 1111
        System.out.println(Integer.toBinaryString(c ^ a & b)); // 1111 1111 1111
        System.out.println(Integer.toBinaryString(a ^ a & b)); // 0000 1111 0000

        // c = a = expr. --- means that they are all equal
        // priority: &, |, ^, ~, <<, >>, >>> (>>> - fill with zeroes)
        // & - и, конъюнкция
        // | - или, дизъюнкция
        // ^ - сложени по модулю 2 (строгая дизъюнкция)
        //c = a = (a ^ b | a & b) ^ c;

        //System.out.println((c | a) ^ (c & a));
    }

}