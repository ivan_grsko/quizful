package interview.camel_case;

public class ToCamelCase {

    /*
    testLongInput() i<10000;        1.170s, 1.199s, 1.160s
     */
    /*public static String convert(String line) {
        if (line == null)
            return null;
        if (line.isEmpty())
            return "";

        String[] split = line.split("([()\\s;:,.!-])+?(?=([A-Za-zА-Яа-я]))");
        for(int i = 0; i < split.length; i++) {
            String splitI = split[i].replaceAll("[()\\s;:,.!-]+", "");
            split[i] = splitI.length() > 0 ?
                    (splitI.substring(0, 1).toUpperCase() + splitI.substring(1).toLowerCase())
                    : "";
        }
        String join = String.join("", split);
        return join.substring(0, 1).toLowerCase() + join.substring(1);
    }       //*/

    /*
    testLongInput() i<10000;    0.076s, 0.079s,
     */
    public static String convert(String line) {
    if (line == null)
            return null;
        if (line.isEmpty())
            return "";

        StringBuilder result = new StringBuilder();
        boolean isFirstSymbolInWord = false;
        for(char c : line.toCharArray()) {
            if (isSpaceOrPunctuationMark(c)) {
                isFirstSymbolInWord = true;
                continue;
            }
            if (isFirstSymbolInWord == true) {
                result.append(Character.toUpperCase(c));
                isFirstSymbolInWord = false;
            } else {
                result.append(Character.toLowerCase(c));
            }
        }

        String resultLine = result.toString();
        if (resultLine.length() > 0) {
            resultLine =
                    String.valueOf(resultLine.charAt(0)).toLowerCase() +
                            resultLine.substring(1);
        }
        return resultLine;
    }

    private static boolean isSpaceOrPunctuationMark(char c) {
        if (c == " ".charAt(0) ||
                c == "(".charAt(0) ||
                c == ")".charAt(0) ||
                c == ",".charAt(0) ||
                c == ".".charAt(0) ||
                c == ":".charAt(0) ||
                c == ";".charAt(0) ||
                c == "!".charAt(0) ||
                c == "-".charAt(0)) {
            return true;
        }
        return false;
    }       //*/



    /* E.K. implementation
    testLongInput() i<10000;    0.398, 0.373s, 0.367s, 0.357s, 0.360s
     */
    /*public static String convert(String line) {
        if (line == null)
            return null;
        if (line.isEmpty())
            return "";
        StringBuilder result = new StringBuilder();
        String[] split = line.trim().split("[()\\s,:;!.-]{1,}");
        for (String s : split) {
            if (s == null || s.length() == 0) {
                continue;
            }
            result.append(String.valueOf(s.charAt(0)).toUpperCase());
            result.append(s.substring(1).toLowerCase());
        }

        String resultLine = result.toString();
        if (resultLine.length() > 0) {
            resultLine =
                    String.valueOf(resultLine.charAt(0)).toLowerCase() +
                    resultLine.substring(1);
        }
        return resultLine;
    }       //*/

}
