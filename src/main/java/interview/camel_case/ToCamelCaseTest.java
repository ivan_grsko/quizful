package interview.camel_case;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ToCamelCaseTest {

    // test: null
    // test: empty line ""
    // test: spacers "     "


    @Test
    public void testOneSymbol() {
        String stringToProcess = "h";
        String converted = ToCamelCase.convert(stringToProcess);
        Assert.assertEquals(converted, "h", "stringToProcess = '" + stringToProcess + "'");
    }

    @Test
    public void testHelloWorld() {
        String stringToProcess = "hello, world!";
        String converted = ToCamelCase.convert(stringToProcess);
        Assert.assertEquals(converted, "helloWorld", "stringToProcess = '" + stringToProcess + "'");
    }

    @Test
    public void testSpacesAtLineEnds() {
        String stringToProcess = "   пробелы в начале и в конце   ";
        String converted = ToCamelCase.convert(stringToProcess);
        Assert.assertEquals(converted, "пробелыВНачалеИВКонце", "stringToProcess = '" + stringToProcess + "'");
    }

    @Test
    public void testLeadingPunctuation() {
        String stringToProcess = "!!!Внимание: важная информация";
        String converted = ToCamelCase.convert(stringToProcess);
        Assert.assertEquals(converted, "вниманиеВажнаяИнформация", "stringToProcess = '" + stringToProcess + "'");
    }

    @Test
    public void testLongInput() {
        String stringToProcess = new String("Так говорила в июле 1805 года известная Анна Павловна Шерер, фрейлина и приближенная императрицы Марии Феодоровны, встречая важного и чиновного князя Василия, первого приехавшего на ее вечер. Анна Павловна кашляла несколько дней, у нее был грипп, как она говорила (грипп был тогда новое слово, употреблявшееся только редкими). В записочках, разосланных утром с красным лакеем, было написано без различия во всех");
        String stringToProcessToAdd = new String("Так говорила в июле 1805 года известная Анна Павловна Шерер, фрейлина и приближенная императрицы Марии Феодоровны, встречая важного и чиновного князя Василия, первого приехавшего на ее вечер. Анна Павловна кашляла несколько дней, у нее был грипп, как она говорила (грипп был тогда новое слово, употреблявшееся только редкими). В записочках, разосланных утром с красным лакеем, было написано без различия во всех");
        String expected = new String("такГоворилаВИюле1805ГодаИзвестнаяАннаПавловнаШерерФрейлинаИПриближеннаяИмператрицыМарииФеодоровныВстречаяВажногоИЧиновногоКнязяВасилияПервогоПриехавшегоНаЕеВечерАннаПавловнаКашлялаНесколькоДнейУНееБылГриппКакОнаГоворилаГриппБылТогдаНовоеСловоУпотреблявшеесяТолькоРедкимиВЗаписочкахРазосланныхУтромСКраснымЛакеемБылоНаписаноБезРазличияВоВсех");
        String expectedToAdd = new String("такГоворилаВИюле1805ГодаИзвестнаяАннаПавловнаШерерФрейлинаИПриближеннаяИмператрицыМарииФеодоровныВстречаяВажногоИЧиновногоКнязяВасилияПервогоПриехавшегоНаЕеВечерАннаПавловнаКашлялаНесколькоДнейУНееБылГриппКакОнаГоворилаГриппБылТогдаНовоеСловоУпотреблявшеесяТолькоРедкимиВЗаписочкахРазосланныхУтромСКраснымЛакеемБылоНаписаноБезРазличияВоВсех");

        // i = 10000: 0.398, 0.373s, 0.367s, 0.357
        for (int i = 0; i < 10000; i++) {
            stringToProcess += stringToProcessToAdd;
            expected += expectedToAdd;
        }

        long start = System.currentTimeMillis();
        String converted = ToCamelCase.convert(stringToProcess);
        long end = System.currentTimeMillis();
        System.out.println("testLongInput convert() time = " + ((end - start)/1000.0) + "s.");
        Assert.assertEquals(converted, expected, "stringToProcess = '" + stringToProcess + "'");
    }

    // test: with tabs ?
}
