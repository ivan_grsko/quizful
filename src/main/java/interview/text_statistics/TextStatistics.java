package interview.text_statistics;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class TextStatistics {

    public List<Word> get(String text) {
        LinkedHashMap<String, Integer> wordsCount = new LinkedHashMap<>();

        String[] split = text.split("(\\s-\\s|-\\s|[\\s,.()!?:;])+");
        for (String s : split) {
            String wordLowered = s.toLowerCase();
            if (wordsCount.containsKey(wordLowered)) {
                Integer count = wordsCount.get(wordLowered);
                wordsCount.put(wordLowered, count + 1);
            } else {
                wordsCount.put(wordLowered, 1);
            }
        }

        ArrayList<Word> words = new ArrayList<>();
        wordsCount.entrySet().stream()
                .sorted((w1, w2) -> (Integer.compare(w2.getValue(), w1.getValue())))    // more simple, but not the order we need: .sorted(Map.Entry.comparingByValue())
                .forEach(entry -> words.add(new Word(entry.getKey(), entry.getValue())));
        return words;
    }



}
