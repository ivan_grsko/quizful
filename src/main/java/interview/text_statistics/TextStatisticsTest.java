package interview.text_statistics;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

public class TextStatisticsTest {

    TextStatistics aut = new TextStatistics();

    // что делать с: А.С.Пушкин - одно слово или отбрасывать "А.С."???
    // что делать с: именами собственными (в данной реализации они сохраняются с нижним регистром первой буквы)
    //               другая реализация будет значительно сложнее

    @Test
    public void testAll() {
        String test =
                "Так говорила в июле 1805 года известная Анна Павловна Шерер, фрейлина и приближенная императрицы " +
                        "Марии Феодоровны, встречая важного и чиновного князя Василия, первого приехавшего на ее " +
                        "вечер. Анна Павловна кашляла несколько дней, у нее был грипп, как она говорила (грипп был " +
                        "тогда новое слово, употреблявшееся только редкими). В записочках, разосланных утром с " +
                        "красным лакеем, было написано без различия во всех. Точно? Точно?! Точно!..\n" +
                        "И ещё немного тестового текста. Писатели: Салтыков-Щедрин, А.С.Пушкин.\n" +
                        "Я - человек;\n" +
                        "- Привет!\n" +
                        "- Здоров!\n" +
                        "- Чо как оно?\n" +
                        "- Да... Так как-то, в общем то ничего нового.\n";

        List<Word> words = aut.get(test);

        Assert.assertEquals(words.size(), 74, "Проверка кол-ва уникальных слов");

        // three occurrences check:
        HashMap<String, Integer> wordsThreeOccurExpected = new HashMap<>();
        wordsThreeOccurExpected.put("точно", 3);
        wordsThreeOccurExpected.put("в", 3);
        wordsThreeOccurExpected.put("и", 3);

        HashMap<String, Integer> wordsThreeOccurActual = new HashMap<>();
        wordsThreeOccurActual.put(words.get(0).getWord(), words.get(0).getOccurrences());
        wordsThreeOccurActual.put(words.get(1).getWord(), words.get(1).getOccurrences());
        wordsThreeOccurActual.put(words.get(2).getWord(), words.get(2).getOccurrences());
        Assert.assertEquals(wordsThreeOccurActual, wordsThreeOccurExpected);

        // two occurrences check:
        HashMap<String, Integer> wordsTwoOccurExpected = new HashMap<>();
        String[] twoOccurArr = {"так", "говорила", "анна", "павловна", "был", "грипп", "как", "с"};
        for (int i = 0; i < twoOccurArr.length; i++) {
            wordsTwoOccurExpected.put(twoOccurArr[i], 2);
        }
        HashMap<String, Integer> wordsTwoOccurActual = new HashMap<>();
        for (int i = 3; i < 11; i++) {
            wordsTwoOccurActual.put(words.get(i).getWord(), words.get(i).getOccurrences());
        }
        Assert.assertEquals(wordsTwoOccurActual, wordsTwoOccurExpected);

        // one occurrence check:
        // ...
    }

    // many small and sharp tests :)
    @Test
    public void testSemicolon() {
        //...
    }
    //...
}
