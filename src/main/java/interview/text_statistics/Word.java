package interview.text_statistics;

public class Word {
    String word;
    int occurrences;

    public String getWord() {
        return word;
    }

    public int getOccurrences() {
        return occurrences;
    }


    public Word(String word, int occurrences) {
        this.word = word;
        this.occurrences = occurrences;
    }

    @Override
    public String toString() {
        return "{" + word + ": " + occurrences + "}";
    }
}
