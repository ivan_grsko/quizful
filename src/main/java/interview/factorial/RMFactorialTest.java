package interview.factorial;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RMFactorialTest {

    RMFactorial rmFactorial = new RMFactorial();

    /*@Test
    public void testSimpleFactorial() {
        int simpleFactorial = rmFactorial.simpleFactorial(4);
        Assert.assertEquals(simpleFactorial, 2*3*4);
    }*/

    @Test
    public void testRMEqual() {
        int simpleFactorial = rmFactorial.get(4, 4);
        Assert.assertEquals(simpleFactorial, (2*3*4)*(2*3*4));
    }

    @Test
    public void testRIsOne() {
        int simpleFactorial = rmFactorial.get(1, 4);
        Assert.assertEquals(simpleFactorial, (2*3*4)*(1)*(2*3));
    }

    @Test
    public void testRMiddleOfM_MIsOdd() {
        int simpleFactorial = rmFactorial.get(3, 5);
        Assert.assertEquals(simpleFactorial, (2*3*4*5)*(2*3)*(2));
    }

    @Test
    public void testRMiddleOfM_MIsEven() {
        int simpleFactorial = rmFactorial.get(3, 6);
        Assert.assertEquals(simpleFactorial, (2*3*4*5*6)*(2*3)*(2*3));
    }
}
