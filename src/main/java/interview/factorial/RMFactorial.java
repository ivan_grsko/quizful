package interview.factorial;

public class RMFactorial {

    /*public int simpleFactorial(int n) {
        if (n == 1) {
            return 1;
        }
        return n * (simpleFactorial(n - 1));
    }*/

    /**
     * @param r
     * @param m
     * @return = m!r!(m-r)!
     */
    // r <= m / m is the largest number or equal to r
    public int get(int r, int m) {
        return getNRM(m, r, m);
    }

    private int getNRM(int n, int r, int m) {
        if (n == 1) {
            return 1;
        }
        if (n > r && n > (m-r)) {
            return n * (getNRM(n-1, r, m));
        } else if (n > r || n > (m-r)) {
            return n * n * (getNRM(n - 1, r, m));
        } else {
            return n * n * n * (getNRM(n - 1, r, m));
        }

    }

}
