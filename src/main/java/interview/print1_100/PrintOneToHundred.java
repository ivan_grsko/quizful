package interview.print1_100;

public class PrintOneToHundred {

    public void print() {
        System.out.println(getString());
    }
    private String getString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= 100; i++) {
            int mod2 = i % 2;
            int mod7 = i % 7;
            if (mod2 == 0 && mod7 == 0) {
                stringBuilder.append("TwoSeven\n");
            } else if (mod2 == 0) {
                stringBuilder.append("Two\n");
            } else if (mod7 == 0) {
                stringBuilder.append("Seven\n");
            } else {
                stringBuilder.append(i + "\n");
            }

        }
        return stringBuilder.toString();
    }

}
